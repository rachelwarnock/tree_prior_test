
cd exp_alpha_est

for i in *_n300_*
do
bsub -W 120:00 /cluster/home/warnockr/revbayes/projects/cmake/rb $i >> job.log
done

for i in *_n30_*
do
bsub -W 120:00 /cluster/home/warnockr/revbayes/projects/cmake/rb $i >> job.log
done

cd ../exp_alpha_fixed

for i in *_n300_*
do
bsub -W 120:00 /cluster/home/warnockr/revbayes/projects/cmake/rb $i >> job.log
done

for i in *_n30_*
do
bsub -W 24:00 /cluster/home/warnockr/revbayes/projects/cmake/rb $i >> job.log
done

cd ../exp_dirichlet

for i in *_n300_*
do
bsub -W 120:00 /cluster/home/warnockr/revbayes/projects/cmake/rb $i >> job.log
done

for i in *_n30_*
do
bsub -W 120:00 /cluster/home/warnockr/revbayes/projects/cmake/rb $i >> job.log
done

cd ../mrbayes

for i in *_n30_*
do
bsub -W 120:00 /cluster/home/warnockr/MrBayes/src/mb $i >> job.log
done

for i in *_n300_*
do
bsub -W 120:00 /cluster/home/warnockr/MrBayes/src/mb $i >> job.log
done

cd ../

