# modify mrbayes files


## default branch length priors

cd mrbayes

mb=unconstrained_tree_inference_dir_mrbayes.nex

nchar=300

for i in `seq 1 50`
do
file=mrbayes_n${nchar}_${i}.nex
cp ../data/n$nchar/rep_$i.nex $file
cat ../templates/$mb >> $file
done

nchar=30

for i in `seq 1 50`
do
file=mrbayes_n${nchar}_${i}.nex
cp ../data/n$nchar/rep_$i.nex $file
cat ../templates/$mb >> $file
done

cd ../
